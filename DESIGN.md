# Saw Loser - Design

I started with a design for a fairly standard two wheel drive vertical drisk bot inspired mostly by Magnetar/Pulsar. I knocked up a quick design for it, ordered some tapered roller bearings and other parts to make it go (I was originally intending to go brushless hub drive) and 3d printed it. After fitting the motors and driving it around a bit and looking at the proportions, I wasn't very happy with it, just looked kinda boring and drove pretty damn badly.

![White Dwarf](docs/whitedwarf.png)

So, next I though about trying an overhead saw bot ala Saw Blaze. I decided I wanted 4wd after drive experiments with the above thing, also I saw Reality/Bequinox and decided I wanted a sorta motor in weapon design (Audience: but weren't you making an overhead saw bot?), which led to me having more of a disk than a saw.

## Parts

NOTE: I've tried to put things in the order that I designed them, however, it's worth noting that most of these parts changed over the design of the whole robot as I realised issues with elements of it. The components in the pictures below are the final ones (in V1) rather than the aprts as they were when first designed. 

So I still had those tapered roller bearings (TRBs), so started designing the whole thing around that. Note: you normally use them in pairs but my thought was the TRB would take most of the load and the motor bearing would take up the thrust load and prevent the TRB from flying apart.

First things first, the weapon motor mount. I already had a Propdrive 2826S motor so that's what I designed these around.

![weapon motor mount](docs/motormount.png)

This was probably one of the hardest parts to design, as it was my first time building something more than a box Fusion 360. Also I had to fit a lot in to a part that does quite a lot. It has to:
* Support the TRB and weapon disk
* Allow the motor cables to pass through the centre of the bearing
* Fix the motor in place
* Fix the whole weapon assembly to the arm
* Not let the static parts of the weapon assembly rotate on the arm 
* Be printable
* Not explode

A shoulder bolt provides most if the strength in the part and holds this part plus the rest of the assembly to the arm.

Next the all important disk, something not too big, I thought about 100g seemed about right, a bit chunky 5mm thick, needs a 40mm hole in the middle for the TRB, I'd like it to look like a saw, so more teeth than you might expect on a traditional vertical disk, maybe 6. The rest of the dimensions just kinda fell out from those requirements.

![disk](docs/disk.png)

Next up we have the power transfer cup and retaining ring for the disk. The power transfer cup needs to contain the motor and provide some protection for it. It will also be in tension holding the TRB together.

![power transfer cup](docs/powertransfercup.png)

The arm to support and move the weapon had a few different things it needed to do:
* Have a shape that interfaces with the weapon assembly to prevent rotation 
* Hold the weapon assembly securely to the arm
* Provide a channel for the weapon motor wires to pass through from the weapon assembly
* Provide a channel for the weapon ESC wires to pass through from the "inside" of the arm
* Provide a "foot" to absorb the downward force from when the weapon hits, so that we're not transferring that force back through the arm 
* Integrated worm wheel to move the arm
* Easily replaceable feeder wedge

![arm](docs/arm.png)

Here's  the weapon assembly and arm all stuck together (you can see the, now fully enclosed, weapon motor mount highlighted in blue)

![arm weapon assembly](docs/armweaponassembly.png)

The worm gear and arm were designed together as they are somewhat reliant on each other for positioning in the main body and we want to make sure the worm meshes nicely with the arm gear. This was a somewhat trial and error process, using boolean operations to cut away a single "tooth" of the worm from the blank cylinder of what would become the worm wheel, then repeating that cutout fetaure in a circular pattern arond the cylinder to create a complete worm wheel. Extensive use of cross section analysis was used to to ensure the worm meshed okay with the worm wheel on the arm.

![worm cross section](docs/gearcrosssection.png)

The worm, like quite a lot of parts in Saw Loser, uses a "nut trap" for a square m3 nut to help retain the grub screw that fixes the worm to the arm motors gearbox shaft.

![worm](docs/worm.png)

The loads the worm will see are mostly thrust, so we've got a thrust bearing at each end of the worm, with the gearbox and a roller (probably overkill) at the other end.

![worm bearings](docs/wormbearings.png)

Next up I designed some wheels, I wanted 4wd, but didn't want 4 motors and gearboxes, so decided to got with wheels with integrated spur gears and a central gear driving both. This design would put the output gear outside the body of the robot but the wheels should mostly protect it. 

The wheel cores are inspired by [Charles Guan's on Uberclocker](http://www.etotheipiplusone.net/?p=4259)

![beetle wheel](docs/beetlewheel.png)

I designed a quick test chassis to make sure the wheel/drive design would drive okay.

![derp chassis](docs/derpchassis.png)

I made up some moulds for the tyres, since they would not be 3d printed. I also made up a frame to mount 4 of the moulds and wheels onto, to make production of the wheels a bit quicker.

![tyre mould](docs/tyremould.png) 

![tyre mould frame](docs/tyremouldframe.png)

I'd decided brushless hub drive probably wasn't for this robot, it needed a bit more precision control, so I went with some fairly standard 25mm gearmotors, as I was planning on still running brushless motors (2300kv 1806 sized) to save some weight, I went for the 200/120rpm models (which are about 44:1 reduction) rather than the more standard 1000 rpm ones (which are more like 11:1).

I modelled up the gearmotors as just solid bits just for checking fit, I actually never bothered modelling them properly/getting the weights right.

With the arm, wheels and gearmotors decided on I could lay things out then start designing the body around those components.

![pre body](docs/prebody.png)

So I just started off with a rough idea of what I wanted, a slightly sloped front (not too wedgey, as we want the arm sticking out well in front). Mounting blocks for the arm and arm drive system and hopefully "enough" room for the other parts (a rectangular mass is seen here at the back as a substitute for a proper model of a battery)

![body 1](docs/body1.png)

Next was fleshing out the blocks to mount the worm drive system and join it all up with the left arm mounting post, so we've got a nice solid core to keep the worm and drive all aligned with each other.

![bearing blocks](docs/bearingblocks.png)

Next up, mounting points for the wheel dead shafts and the gearmotors, and cutout in the right hand arm post to allow the gearmotor through, this weakens the post but it is now quite beefy, so should be okay. 

Spacing for the gears is fairly easy here as I'm using mod1 gears on the wheels and for the 3d printed "pinion". Both the pinion and wheel gears have 28 teeth, to the spacing between centres is  56mm (+ a little bit 0.1mm or so for clearance due to 3d printing mishaps).

![body 2](docs/body2.png)

A few more features, including a battery box, you'll notice a cut out at the back, which doesn't line up with the cutout at the front, this is because the arm mounting is quite far forward in the boddy, so the arm passes through the cutout in the front and with the arm in the back position the cutout is for the disk, so we don't cut into ourselves... There's also a little more chamferring going on around that arm mounting area. Also if you look closely you'll see I've started adding wiring channels, so that we can thread wires under the worm gear between sides of the robot.

![body 3](docs/body3.png)

With the main features in there most of the rest of the body design was adding in nice things such as:
* Mounting points for possible additional front and rear armour configurations
* Cable tie holes to keep all the wiring in check
* Walls around the brushless drive motors to prevent wires getting stuck
* A mounting point at the rear right for the xt60 link socket
* Mounting holes and nut traps for the battery box lid and main lid (probably too many)
* Loads more chamfer/filleting

![body complete](docs/bodycomplete.png)

With the arm, lids and other printed components in place it's looking quite tight in there, but electronics and wires don't take up any room, right?

![body complete](docs/bodycomplete2.png)

So with the body complete we need some armour, so all the bits stay in. The top is realtively simple, just following the top line of the body around and splitting in two to allow the arm to move just under 180 degrees from front to back. The idea was to print a lid in nylon, however then I saw a bunch of overhead saws and axes, got scared and decided to get a ti lid waterjet cut. The front armour was designed once the I'd got everything printed and all components in, I wanted it to be wider with a shallower angle on the sides to force horizontals spinners up, but it's the largest bit of 3mm thick ti I could get in the weight, so barely covering the front face will have to do.

![ti armour](docs/tiarmour.png)

With the front armour not being all it could be I thought I'd design a couple of optional armour pieces for the back if I went up against a nasty horizontal or undercutter.

![protect armour](docs/protect.png)

![hug armour](docs/hug.png)

And we're done!