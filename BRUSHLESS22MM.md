# How to make brushless 22mm planetary gear motors

## Things you'll need (for one complete brushless gear motor)

* 2 x "Rotalink 22mm Planetary Gearmotors"
  - https://www.ebay.co.uk/itm/DC-6V-18V-12V-480RPM-Micro-Full-Metal-Gear-Motor-Planetary-Gearbox-DIY-Robot-Car/192241458644
  - https://www.ebay.co.uk/itm/Rotalink-DC-12V-18V-700RPM-Full-Metal-Planetary-Gearbox-Gear-Motor-DIY-Robot-Car/264423999404
* 1 x DYS BE1806 2300kv brushless  motor (make sure to get the one with 2mm rather than 3mm shaft)
* 4 x M2 x 5mm socket head bolts
* 1 x M2 x 10mm (ish) bolt
* Screwdriver with the following bits:
  - Philips PH0
  - Torx T6
  - Torx T8
* Dremel or Hacksaw
* Pliers
* Side cutters
* Arbor Press or maybe a vice
* Threadlocker (Loctite or similar)

## Getting the shaft and pinion from the original motor

Remove the mounting bracket from the front (not pictured) using a T6 bit, then remove the 4 screws that hold the gearbox to the motor mounting plate.
![01](docs/gearmotor/01.jpg)
Put the gearbox and bolts somewhere safe, you'll need them later
![02](docs/gearmotor/02.jpg)
Remove the motor mounting plate with a T8 bit. Keep the plate, but you can dispose of the T8 screws, since they wont fit the brushless motor.
![03](docs/gearmotor/03.jpg)
Okay, so now we've got the motor, we will be discarding most of it, we just want the shaft with its firmly fitted pinion.
![04](docs/gearmotor/04.jpg)
Now is a good time to measure the distance from the front of the pinion to the front of the motor (this is important later, as we will need to replicate this when the shaft is installed in the brushless motor)
![05](docs/gearmotor/05.jpg)
5.66mm :thumbsup:
![06](docs/gearmotor/06.jpg)
To remove the brushes and back of the motor, bend up the two tabs either side with a small flat bladed screw driver.
![07](docs/gearmotor/07.jpg)
You can dispose of the plastic back and brushes.
![08](docs/gearmotor/08.jpg)
Pull the commutator (flimsy plastic, carbon and copper thing) off the back of the shaft with some pliers.
![09](docs/gearmotor/09.jpg)
Now we need to cut away the windings, so we can remove the plates that make up the rotor.
![10](docs/gearmotor/10.jpg)
Cut through the copper windings with some side cutters.
![11](docs/gearmotor/11.jpg)
Cut.
![12](docs/gearmotor/12.jpg)
Pull the windings out
![13](docs/gearmotor/13.jpg)
Repeat until all the windings are removed.
![14](docs/gearmotor/14.jpg)
Pull out the rotor, one piece at a time, you may have to wiggle them a bit to pull them up off the shaft.
![15](docs/gearmotor/15.jpg)
You should end up with something like this.
![16](docs/gearmotor/16.jpg)
Get a bolt and put its head over the shaft, this will allow you to push the shaft out far enough to remove the little brass collar holding the thing together. It's a good idea to hold on to the bolt while pressing the shaft out to make sure that the force is being applied vertically, so that the shaft does not get bent.
![17](docs/gearmotor/17.jpg)
Shaft pushed out as far as we can, since the shaft is being held in by a little brass collar
![18](docs/gearmotor/18.jpg)
Push the shaft back through the front of the motor and you should see the little collar. Pull it off the shaft with a pair of pliers.
![19](docs/gearmotor/19.jpg)
The shaft is now free to come out of the front of the motor. You can now discard the motor casing.
![20](docs/gearmotor/20.jpg)
Pile of un-needed parts.
![21](docs/gearmotor/21.jpg)

## Putting the shaft and pinion from the original motor into your brushless motor

My brushless motor already has a shaft and pinion from another brushed motor (one of the 25mm crappy spur gearboxes, which are not really suitable for brushless conversion). The same principles apply with a fresh motor from DYS.

For a fresh motor you will need to remove the circlip on the front of the motor. Dont worry about completely mangling it, as you will not need it again.

Also on a fresh motor, once the circlip is removed, you can remove the rotor (can't do this with my premodified one, as the pinion prevents the removal)
![22](docs/gearmotor/22.jpg)
To get the shaft out of the rotor, you'll need to use something to press it out, I use an M2x10mm socket head bolt, as it's slightly smaller than the 2mm shaft (if you've got a fresh motor, I'd suggest doing this to the rotor only, to prevent possible damage to the bearings, you can leave the stator to one side).

Don't worry about removing the tiny grubscrew in the rotor, you will destroy allen keys trying to remove it, and it doesn't do very much.
![23](docs/gearmotor/23.jpg)
Make sure the bolt is perpendicular and push the shaft out of the rotor.
![24](docs/gearmotor/24.jpg)
The old shaft can be discarded.
![25](docs/gearmotor/25.jpg)
One shaft free brushless motor.
![27](docs/gearmotor/27.jpg)
Take the shaft and pinion you extracted from the Rotalink motor and push it through the stator and a little into the rotor, by hand.
![28](docs/gearmotor/28.jpg)
You can use the motor mounting plate to help pressing the shaft in, using it to keep the rotor square aginst the base of the arbor press.
![29](docs/gearmotor/29.jpg)
Squish
![30](docs/gearmotor/30.jpg)
Once the shaft is through, you can employ some calipers to help you get the spacing from the front of the pinion to the front of the motor.
![32](docs/gearmotor/32.jpg)
5.63mm is almost the same as 5.66mm, close enough anyway,
![33](docs/gearmotor/33.jpg)
So now the motor is done, you could leave it there, but it does have a big shaft sticking out the back. So we're going to remove that.
![34](docs/gearmotor/34.jpg)
Tape the motor up, the shaft is made of steel and the rotor is very magnetic, and you'll end up with bits of metal in your nice new motor if you don't.
![35](docs/gearmotor/35.jpg)
Dremel or hacksaw off the extra shaft length
![36](docs/gearmotor/36.jpg)
Still a little sticking out the back
![37](docs/gearmotor/37.jpg)
We are going to mount the brushless motor to the original motor's mounting plate, but you can see by default we can't quite use all 4 mounting holes. The ones that are closer together need a little slotting towards the centre, in order to let us get a couple more M2 bolts in there.
![38](docs/gearmotor/38.jpg)
Use a tiny round file, doesn't take much, just a few seconds of filing.
![39](docs/gearmotor/39.jpg)
Remember to Loctite your bolts, don't want the motor falling off.
![40](docs/gearmotor/40.jpg)
All mounted
![41](docs/gearmotor/41.jpg)
You could now attach the motor back to the gearbox, but you probably want to follow the next stage of this guide first.
![42](docs/gearmotor/42.jpg)

## Replacing the first stage nylon gears with metal gears

The Rotalink gearboxes are very cheap and are pretty decent, however the first set of planet gears are made of nylon rather than steel. With the increased power of the brushless motor, these will not last that long.

So we can take a second gearbox, take the steel gears from the 2nd stage and use those to replace the nylon first stage.

Pay attention to how the gearbox comes apart.
![43](docs/gearmotor/43.jpg)
Nylon gears in the first stage
![44](docs/gearmotor/44.jpg)
Steel gears in the second stage
![45](docs/gearmotor/45.jpg)
Take it all apart, now might be a good time to clean it all (use some isopropyl alcohol or similar). Do remember to regrease it if you do give it a good clean (lithium grease has worked fine for me)
![46](docs/gearmotor/46.jpg)
Take a second gearbox apart
![47](docs/gearmotor/47.jpg)
Extract the steel gears from the second stage.
![48](docs/gearmotor/48.jpg)
Rebuild (and regrease) the planet carriers with all steel gears
![49](docs/gearmotor/49.jpg)
Put it all back together
![50](docs/gearmotor/50.jpg)
Don't forget the thin steel plate in between the first stage and the motor mounting plate
![51](docs/gearmotor/51.jpg)
Fix the gearbox to the motor mounting plate (don't forget the threadlocker)
![52](docs/gearmotor/52.jpg)
One sturdy, fairly cheap brushless gearmotor.
![53](docs/gearmotor/53.jpg)
