# Saw Loser - 3D Printing

You'll find the STLs in the [STLs folder](stls/)

All the printed parts you need for one whole Saw Loser are shown in the print plate picture below (you probably can't/don't want to print all the parts at once). The orientation for printing all the parts can also be seen

![print plate](docs/printplate.png)

Most parts were printed in Taulman 910 nylon filament, using 0.6mm nozzle, 0.3 layer height. For gears/geared parts a 0.4mm nozzle and 0.2/0.25mm layers were used.

The parts that I printed with a 0.4mm nozzle are:
* Pinions
* Wheel cores
* Weapon motor mount

Hopefully you should be able to see from the picture above which STLs will require multiples.