# Saw Loser - ESCs

## Drive and Arm ESCs

I used some really cheap (£1.50) KingKong brand 12A brushless ESCs for drive. 

The ESCs come with a BLHeli bootloader and BLHeli firmware. THe firmware is configured for single direction operation out of the bag.

To flash these with something else you'll need:
* An Arduino Nano (to make a BLHelisuite 4-way interface) - e.g. [These clones on Amazon](https://www.amazon.co.uk/ELEGOO-Arduino-board-ATmega328P-compatible/dp/B072BMYZ18)
* A little bit of wire
* A soldering iron
* BLHeliSuite - Download from the [official github page](https://github.com/4712/BLHeliSuite/releases)
* Bi-Directional SimonK firmware - Download from my repo [bidirectional_bs_nfet.hex](https://gitlab.com/alexmordue/kingkong12a-esc-firmwares/raw/master/SimonK/bidirectional_bs_nfet.hex)

### Build the 4-way interface (simplified for bootloader use only)

Hardware
* Snap off 3-pins of the 0.1" header that comes with the Arduino Nano board
* Solder the 1st pin with some wire to D3 on the Nano board
* Solder the 3rd pin with some wire to GND on the Nano board
* Mark the GND connected pin in some way to make it easy to identify

Programming up the Arduino 
* Connect up the Arduino to your PC
* Install the CH340G drivers (if needed, also worth noting that your nano may come with a different USB serial chip e.g. ft232)
* Check in device manager to see what Serial port your new device has been assigned and make note of it, it should look like `COM4` or similar (the easiest way to do this is just to unplug and replug the device with device manager open and see what appears and disappears)
* Run BLHeliSuite
* Go to the `Make Interfaces` tab
* Choose from `Nano w/ Atmega328` from `Make Arduino Interface Boards` 
* Choose the Serial port you found out earlier
* Click `Arduino 4way-interface` and BLHeliSuite should program up your Arduino as an ESC programming interface

The 4way-interface is useful, as we can use it with ESCs that have BLHeli or SimonK bootloaders, also it works with Atmel and SiLabs based ESCs, so it's the most flexible type. You can buy ESC programming interfaces from HobbyKing etc. but I've had most success with just using an Arduino Nano programmed up as a BLHeliSuite 4way-interface. 

### Flashing firmware to a KingKong ESC

* Connect the arduino interface to your computer, if not already connected 
* Connect your ESC up to a 12v supply (and nothing else yet)
* Connect the 3-pin connector from your 4way-interface to the servo plug on the ESC, making sure to align the GND pin to the black wire of the ESC
* Start BLHeliSuite (if not already started)
* The KingKong 12A ESC is running the BLHeli bootloader and an Atmel processor and we have a 4way-interface so select `ATMEL BLHeli Bootloader (4way-if)` from the interface menu at the top.
* Click on the `Atmel ESC Setup` tab (if it's not already on that tab)
* Select the serial port of the Arduino Interface (use the device manager trick from the previous section if you dont know which one it is and have more than one port listed)
* CLick `Connect`
* Click `Flash Other` and choose the hex file from above

Things to try if it doesn't work:
* You may need to disconnect the power to ESC and reapply it. 
* You may need to restart BLHeliSuite
* You may need to flash BLHeli again before it will allow you to `flash other`


## Weapon

Mainly just a stock Afro 30A, I think I maybe tweaked it with slightly increased startup power, but the default is fine. The default has no brake and single direction which is perfect for a spinning weapon.