# Saw Loser - Assembly

You should end up with something like this internally:

![internally complete](docs/internallycomplete.jpg)

Rough order of assembly
* [Print everything](PRINTING.md)
* Get disk(s) waterjet
* Get a power transfer cup CNC'ed (alternatively use the 3d printed one from v1)
* Install M3 nuts in all nut traps in printed parts and heat set inserts in body
* Make your wheels
* Make your wiring harness
* Install wiring harness
* Install drive motors
* Install drive pinions
* Install weapon ESC
* Install worm drive assembly
* Build weapon assembly and mount to arm
* Wire up arm
* Install drive ESCs
* Install arm and weapon
* Install wheels

## Bill of Materials

In addition to all the 3D printed parts, you'll need:

### Bolts and nuts
All bolts are allen heads:
* 10 x M3 10mm Socket Head Bolt (lid)
* 8 x M3 25mm Socket Head Bolt (Battery cover and worm assembly lids)
* 4 x M3 8mm Socket Head Bolt (Weapon motor mounting)
* 4 x M3 8mm Countersunk Bolt (Drive Gearbox mounting)
* 1 x M3 25mm Countersunk Bolt (link socket clamp)
* 1 x M3 25mm Socket Head Bolt (Feeder wedge mounting)
* 1 x M4 25mm Socket Head Bolt (Feeder wedge mounting)
* 3 x M3 5mm Grub screw (Pinions and worm)
* ~20 x M3 Square half nuts (all M3 nut traps)
* ~10 x M3 Heat set inserts (for body, for the lid to bolt into)
* 12 x 5mm ID 1mm thick washer (for spacing deadshafts for 4 wheels)
* 4 x 30mm x 5mm (M4 thread) Shoulder bolts (for deadshafts)
* 1 x 35mm x 5mm (M4 thread) Shoulder bolt (for mounting weapon assembly to arm)
* 6 x 20mm x 5mm (M4 thread) Shoulder bolts (disk assembly)
* 4 x 20mm M5 Countersunk bolts (for mounting armour front/rear)
* 10 x M4 nyloc nuts (disk assembly and drive deadshafts)
* 1 x M4 heat set insert (for mounting weapon assembly to arm)
* 1 x M4 square full nut (Feeder wedge mounting)
* 4 x M5 square full nuts (for mounting armour front/rear)

### Bearings
Specified as OD x ID x Width
* 1 x 40x17x13.5 Tapered roller bearing - I used some Dunlop 30203 ones
* 8 x 14x5x5 Sealed roller bearing 605-2RS (two per wheel)
* 1 x 16x8x5 roller bearing - I used 688Z (for supporting the back of the worm)
* 1 x 16x8x5 thrust bearing (for supporting the back of the worm)
* 1 x 10x4x4 thrust bearing (for supporting the front of the worm)
* 2 x 15x10x10 Flanged oilite bushing (flange diameter 21mm, total thickness is 10mm, flange thickness 3mm)

### Motors and electronics
* 3 x DYS BE1806 (with 2mm shaft)
* 1 x 200rpm/120rpm 25mm diameter gearmotor
* 4 x 22mm diameter "rotalink" planetary gearmotors (4 as you harvest the metal gear stage from 2 and use it to make 2 all metal gearboxes) see [my guide for building your own 22mm brushless gearmotors](BRUSHLESS22MM.md)
* 1 x Propdrive 2826S 1000kv/1100kv (short shaft)
* 3 x Kingkong 12A Brushless ESCs [flashed with SimonK reversible FW or similar](ESC.md)
* 1 x Afro 30A Brushless ESC (no reverse and no braking, which is the default)
* 1 x FrSky RX6R receiver (use BEC from weapon ESC)
* 1 x XT60U socket
* 1 x XT60U plug
* 1m 18AWG wire (black and red)
* 1m 16AWG wire (black and red)
* Mini blade fuse (35A) and holder
* 1000 mAh 3S LiPo with current capability of 35C or above
* 5mm, 12V tolerant LED
* Some 2mm and 3mm bullet connectors (how many you need varies on what your ESCs and motors ship with)

### Wheel casting materials
* Polyurethane 2 part rubber, 40A Durometer
* Vaseline for mould release
* PU Pigment (if you want fancy coloured tyres)

### Other
* 300mm x 10mm carbon fibre rod (for arm shaft, much lighter than using a steel 10mm bolt)
* Heatshrink (small maybe get a variety around 2-5mm diameter)
* Cable ties (2-4mm wide)

## Casting wheels
Making polyurethane tyres. The moulds are printed in PLA and brushed with petroleum jelly as a release agent. Many thanks to Charles Guan for the wheel design idea.

![wheel casting 1](docs/wheelcasting1.jpg)

The wheel hubs are clamped down into the moulds to prevent the PU leaking under the wheel faces. As you can see I’ve used these moulds a few times already. I’ve also printed a little frame with some locating pegs for the moulds and hubs, to keep the hubs centred in the mould.

![wheel casting 2](docs/wheelcasting2.jpg) 

The polyurethane rubber comes in two parts: A which is fairly thin (I’ve found it best to mix the pigment into this part) and B which is much thicker. I’ve found it best to measure part B out first as it’s much easier to measure out the thinner part, A, accurately.

![wheel casting 3](docs/wheelcasting3.jpg)

Mixing the two parts needs to happen quite quickly as the PU is only really pourable for a few minutes after mixing starts. I’ve found pouring part A into part B, mixing for 20 seconds then pouring the lot back into the part A cup and mixing for another 10-15 seconds gives a decent mix. Pouring into the moulds is a little tricky, just pour it slowly and don’t worry about wasting a little in between each mould. I like to pop any bubbles that come to the surface in the first couple of minutes.

![wheel casting 4](docs/wheelcasting4.jpg)

Don’t worry about that extra PU on the teeth, it’ll come off with a sharp knife and some pulling.

![wheel casting 5](docs/wheelcasting5.jpg)

## Electronics installation
The wiring is the first thing to go in as the worm assembly and arm go over a lot of the wiring. I like to make up a  wiring harness to which I can just plug in the other parts, to make replacing blown ESCs etc. easier.

![wiring harness](docs/wiringharness.jpg)

Measure up your wires in the body so that the lengths aren't too short/long. As you can probably see from the pictures I've used bullet connectors for plugging in my ESCs and the power cable for the receiver. (3mm for the weapon ESC, 2mm for the drive ESCs)

![wiring harness in body](docs/wiringharnessinbody.jpg)

Once the wiring harness is in you can put in the drive motors, fasten them up and install their pinions, then the the worm drive assembly and the weapon ESC. 

I'd recommend adding the drive ESCs later in the process as they just add a load of extra wires that make things more tricky while putting wheels on etc.

While I've put the wheels on in this picture, I would again, recommend putting those on later, as you'll need the right front tyre off in order to install the arms shaft.

![body and parts](docs/bodyplusparts.jpg)

## Weapon assembly

The rotor is epoxied (see [Robert Cowans guide on battle hardening motors](https://www.youtube.com/watch?v=5MFjzSP5Oiw) ) and pushed into the “power transfer cup” / “minimal motor protection housing” or PTC/MMPH if you like acronyms.

![weapon assembly 1](docs/weaponassembly1.jpg)

Weapon motor mount bolted to the motor's stator and the inner race of the taper roller bearing pressed onto the the mount. There is a captive m4 full nut in the mount, that a shoulder bolt through the arm screws into.

![weapon assembly 2](docs/weaponassembly2.jpg)

The stator in place. The original solid-ish core motor wires have been cut and stranded silicone wires have been soldered in their place so they’re a little more flexible.

![weapon assembly 3](docs/weaponassembly3.jpg)

Disk added to the construction. These disks were water jet cut by [k-cut](https://k-cut.co.uk). The centre hole is a little undersized, I can recommend [carbide burrs](https://www.amazon.co.uk/gp/product/B06Y4MLSST) and a Dremel to clean up the parts and take them to dimension.

![weapon assembly 4](docs/weaponassembly4.jpg)

Retaining ring with bolts in place. Use the 40mm diameter printed ring(s) in between the the retaining ring and the outer bearing race to change the pre-tension on the bearing (if needed)

![weapon assembly 5](docs/weaponassembly5.jpg)

The motor mount and the arm are keyed to prevent rotation and take some of the impact.

![weapon assembly 6](docs/weaponassembly6.jpg)

The completed spinner assembly bolted to the arm.

![weapon assembly 7](docs/weaponassembly7.jpg)