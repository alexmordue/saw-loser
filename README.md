# Saw Loser V2 - Partially 3D Printed Beetleweight Combat Robot

![Saw Loser](docs/sawloser.jpg)

You can explore the model in [Fusion 360 here](https://a360.co/2PIQeDw) 

## Introduction

NOTE: If you're looking for the old version (from the first season of Bugglebots, take a look at the v1 tag for this repository).

Saw Loser is a beetleweight (1.5kg) combat robot originally designed for the Season 1 [Bugglebots](https://www.bugglebots.com) web series.

This project contains all the files you need to print your own copy, but you will need some experience to assemble and operate the robot. Combat robots are dangerous and should not be treated as toys. 

If you make one, let me know, please attribute me, and also, please, don't sell them.

## V2 Changes

* Aluminium CNCed power transfer Cup for the weapon motor, to stop in getting damaged quite so easily
* More reliable drive by switching to 22mm planetary gearboxes and reducing the size of the drive pinion to 20T
* New wheel cores to work with the above change
* New stronger body, removing the nut traps and replacing them with heat set inserts
* New stronger weapon arm

Some parts will be interchangeable between designs, e.g. the full spinner assemblies can be swapped between a v1 or v2 arm, the v1 and v2 arms work on both bodies. In theory you could use the same wheels and pinions from v1 on a v2 body with planetary gearboxes, but not recommended dues to the smaller reduction in the planetaries.

## [Design](DESIGN.md)

I've created a detailed design document about the design process behind Saw Loser, this is not essential if you just want to make one, but should help you with assembly, there's lots of pictures and hopefully someone finds it interesting. Click the [design](DESIGN.md) link above if you'd like to learn more.

## Printing/Manufacturing

### Printing

See [3D printing instructions](PRINTING.md)

### Waterjetting

You'll find the DXFs in the [DXFs folder](dxfs/). The required materials are in the filenames, I got [k-cut](https://k-cut.co.uk/) to do mine.

### CNCing

You'll find the power transfer cup stl and drawings for manufacture in the [cnc folder](cnc/). You should be able to export any other format (e.g. step/iges) from the Fusion 360 model supplied.

## Assembly

See [assembly instructions](ASSEMBLY.md)

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">Saw Loser</span> by <a xmlns:cc="http://creativecommons.org/ns#" href="https://gitlab.com/alexmordue/saw-loser" property="cc:attributionName" rel="cc:attributionURL">Alex Mordue</a> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.<br />Based on a work at <a xmlns:dct="http://purl.org/dc/terms/" href="https://gitlab.com/alexmordue/saw-loser" rel="dct:source">https://gitlab.com/alexmordue/saw-loser</a>.
